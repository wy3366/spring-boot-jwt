package com.gitee.wy3366.jwt;

import io.jsonwebtoken.Claims;

public interface IJwtVerificationSuccess {
    void success(Claims claims);
}
