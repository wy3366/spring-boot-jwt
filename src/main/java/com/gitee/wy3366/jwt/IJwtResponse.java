package com.gitee.wy3366.jwt;

import java.util.Map;

public interface IJwtResponse {
    Map<String, Object> empty();

    Map<String, Object> failed();
}
